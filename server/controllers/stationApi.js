var stationModel=require("../models/stationModel"),
	errCode=require("../conf/error_codes"),
	async  = require('async');
module.exports =
{
    login:function (itm,res) {
        stationModel.login(itm,res);
    },
    getStation:function (typ,res) {
        stationModel.getStation(res);
    },
    deleteStation:function (itm,res) {
        stationModel.deleteStation(itm,res);
    },
    saveStation:function (itm,res) {
        if(itm._id){
            stationModel.updateStation(itm,res);
        }else {
            stationModel.addStation(itm,res);
        }
    },
    rateStation:function (itm,res) {
        stationModel.rateStation(itm,res);
    },
    makePament:function (itm,res) {
        res.send("Your payment is successful")
    }
};