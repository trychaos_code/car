const mong = require('mongodb');
const MongoClient = mong.MongoClient;
const ObjectID = mong.ObjectID;
const conf = require('../conf/config');

const jwt  = require('jsonwebtoken');

module.exports = {
    addStation:function (itm,res) {
        MongoClient.connect(conf.development.Local_MONGODB_URI, function(err, db) {
            if(err){
                console.log("err: ",err)
            }
            let collection = db.collection('station');
            try {
                if(!Array.isArray(itm)){
                    itm = [itm];
                }
                let ids = collection.insertMany(itm);
                res.send({
                    msg:"Insertion succeed"
                });
            }catch (pErr){
                console.log("pErr: ",pErr);
                res.send({
                    msg:"Invalid input",
                    err: pErr
                });
            }
        });
    },
    updateStation:function (itm,res) {
        MongoClient.connect(conf.development.Local_MONGODB_URI, function(err, db) {
            if(err){
                console.log("err: ",err)
            }
            let collection = db.collection('station');
            try {
                let idV = new ObjectID(itm._id);
                delete itm._id;
                let ids = collection.updateOne({
                    _id:idV
                },{
                    $set: itm,
                    $currentDate: { lastModified: true }
                });
                res.send({
                    msg:"Updation succeed"
                });
            }catch (pErr){
                console.log("pErr: ",pErr);
                res.send({
                    msg:"Invalid input",
                    err: pErr
                });
            }
        });
    },
    deleteStation:function (itm,res) {
        MongoClient.connect(conf.development.Local_MONGODB_URI, function(err, db) {
            if(err){
                console.log("err: ",err)
            }
            let collection = db.collection('station');
            if(!Array.isArray(itm)){
                itm = [itm];
            }
            itm = itm.map(i=>new ObjectID(i));
            try {
                let ids = collection.deleteMany({_id:{$in: itm}});
                res.send({
                    msg:"Deletion succeed"
                });
            }catch (pErr){
                console.log("pErr: ",pErr);
                res.send({
                    msg:"Invalid input",
                    err: pErr
                });
            }
        });
    },
    rateStation:function (itm,res) {
        MongoClient.connect(conf.development.Local_MONGODB_URI, function(err, db) {
            if(err){
                console.log("err: ",err)
            }
            let collection = db.collection('station');
            collection.updateOne(
                { "_id" : new ObjectID(itm._id) },
                { $set: { "rate" : itm.rate } }
            );
            res.send({
                msg:"Rating succeed"
            });
        });
    },
    getStation:function (res) {
        MongoClient.connect(conf.development.Local_MONGODB_URI, function(err, db) {
            if(err){
                console.log("err: ",err)
            }
            let collection = db.collection('station');
            collection.find({}).toArray(function(err, docs) {
                res.send(docs);
            });
        });
    },
    login:function (itm,res) {
        MongoClient.connect(conf.development.Local_MONGODB_URI, function(err, db) {
            if(err){
                console.log("err: ",err)
            }

            var user = {
                uname:itm.uname,pass:itm.pass
            };
            let token = jwt.sign(user, conf.development.secret,{
                expiresIn: 60*60*5
            });

            let collection = db.collection('user');
            collection.find().toArray(function(err, docs) {
                res.send({token});
            });
        });
    }
};