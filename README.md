* [Overview](#Overview)
    * [Key Feature](#Key_Feature)
    * [Credencials](#Credencials)
    * [Installation](#Installation)
        * [Clone repo](#Clone_repo)
        * [Module installation](#Module_installation)
        * [Run application](#Run_application)
        * [Test application](#Test_application)
* [Browser Support](#Browser_Support)
    * [CSS support](#CSS_support)
    * [Functionality Support](#Functionality_Support)
* [Folder structure](#Folder_structure)
    * [Client](#Client)
    * [Server](#Server)
* [Screenshot](#Screenshot)
    * [Login](#Login)
    * [Dashboard](#Dashboard)
    * [enterAmount](#enterAmount)
    * [bankList](#bankList)
    * [successPayment](#successPayment)
               
            
<a name="Overview"></a>

# Overview

<a name="Key_Feature"></a>

### Key Feature:

   1. MongoDB is being used as database
   2. Filter games has two search functionality
  
    > Fuzzy   
    > Simple    
       
   3. FrontEnd is in Angular(4.2.4) 
   4. Modular backend / Front-End
    
<a name="Credencials"></a>
    
### Credencials

   For logging in to application use following account:
   
   ***UserName***: *ak*
   
   ***Password***: *ak*

<a name="Installation"></a>

### Installation:

<a name="Clone_repo"></a>

#### 1. Clone repo using command
`git clone https://gitlab.com/akhileshcoder/test_payment.git <folder path>`

<a name="Module_installation"></a>

#### 2. Module installation

`cd client`

`npm i`

`cd server`

`npm i`

<a name="Run_application"></a>


#### 3. Run application

>   Client:
>
>   `cd client`
>
>   `npm start`


>   Server:
>
>   `cd server`
>
>   `npm start` or `node website.js`

<a name="Test_application"></a>

#### 3. Test application

>
>   `cd client`
>
>   `npm run test`

<a name="Browser_Support"></a>

# Browser Support

<a name="CSS_support"></a>

### CSS support
![Alt text](doc/img/browser.png?raw=true "Optional Title")

<a name="Functionality_Support"></a>

### Functionality Support (Client JavaScript)
ie-10 and above, All other browser minimum -3 version

<a name="Folder_structure"></a>

# Folder structure:

<a name="Client"></a>

### Client:
![Alt text](doc/img/clientFolder.png?raw=true "Client Folder")

<a name="Server"></a>

### Server:
![Alt text](doc/img/serverFolder.png?raw=true "Server Folder")

<a name="Screenshot"></a>

# Screenshot

<a name="Login"></a>

#### Login Page
![Alt text](doc/img/login.png?raw=true "Login")

<a name="Dashboard"></a>

### Dashboard
![Alt text](doc/img/dashboard.png?raw=true "Dashboard")

<a name="enterAmount"></a>

### Enter Amount
![Alt text](doc/img/enterAmount.png?raw=true "Enter Amount")

<a name="bankList"></a>

### Bank List
![Alt text](doc/img/bankList.png?raw=true "Bank List")

<a name="successPayment"></a>

### Success Payment
![Alt text](doc/img/successPayment.png?raw=true "Success Payment")
