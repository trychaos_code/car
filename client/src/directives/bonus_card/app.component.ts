import { Directive, ElementRef, Input, Output, OnInit } from '@angular/core';

@Directive({
  selector: "[bonusCard]"
})
export class BonusCard implements OnInit{
  @Input() bonus: string;
  @Input() amount: string;
  @Input() custom: string;
  @Input() isSeleted: string;
  @Output() customInp: string;
  constructor(private el: ElementRef){
  }
  ngOnInit(){
    this.el.nativeElement.innerHTML = this.getTemplate()
  }
  getTemplate(){
    return ''+
      '<div class="card-pin">'+
        (
          this.custom ?'' +
          '<div>Enter Amount</div><div class="my-hr"></div>' +
            '<div class="cust-amount">' +
              '<i class="fa fa-rupee"></i>' +
              '<input type="number" placeholder="Enter Amount" [ngModel]="this.customInp" >' +
            '</div>':
          '<div class="bonus '+(this.bonus?"":'invisible')+'"> ' +
            '<div>Bonus</div>' +
            '<div class="bonus-am"><sup><i class="fa fa-rupee"></i></sup>'+this.bonus+'</div>' +
          '</div>'+
          '<div class="amount"><sup><i class="fa fa-rupee"></i></sup>'+this.amount+'</div>'
        )+
        '<div class="radio '+(this.isSeleted?'is_seleted':'')+'"></div>'+
      '</div>' +
      '';
  }
}
