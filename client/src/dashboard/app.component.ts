import { Component } from '@angular/core';
import { Http, Headers } from '@angular/http'
import { Router } from '@angular/router'

@Component({
  selector: 'dashboard',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class Dashboard {
  searchField = {
    txt:"",
    fld:"all"
  };
  searchFuzzy = true;
  edContxt:any = {};
  showGameModel = false;
  gameFildVl = {};
  gameFild = [];
  gameAttribute: any = {
    field:['disc','owner',
      'teamSize','noOfDownloads']
  };
  searchBy='all';
  searchOpt = ['manufacturer','name','price','disc','owner',
    'teamSize','noOfDownloads'];
  http:Http;
  router:Router;
  isLogin = localStorage.getItem('myJWT');
  noOfStar = [0,1,2,3,4];
  station=[];
  stationCopy=[];
  constructor(http:Http,router:Router){
    if(this.isLogin) {
      this.http = http;
      this.router = router;

      this.gameAttribute.label = this.gameAttribute.field.map(i=>i.replace(/([A-Z])/g, ' $1')
        .replace(/^./, str=> str.toUpperCase()));
      this.searchOpt.forEach(i=>{
        let tmp = i.replace(/([A-Z])/g, ' $1')
        .replace(/^./, str=> str.toUpperCase())
        this.gameFild.push(tmp);
      });

      this.getStation();
    }else {
      router.navigateByUrl('auth')
    }
  }
  getStation(){
    let hdr = new Headers();
    hdr.append('jwt', this.isLogin);
    this.http.get('/user/get_station',{headers:hdr}).subscribe(data => {
      let jsn = data.json();
      if(!jsn.errCode){
        this.station = jsn;
        this.stationCopy = JSON.parse(JSON.stringify(this.station));
      }else {
        console.log("Token Expired");
        this.logOut();
      }
    });
  }
  star(id,i,ind){
    let hdr = new Headers();
    hdr.append('jwt', this.isLogin);
    this.http.post('/user/rate_station',
      {_id:id,rate:i},{headers:hdr}).subscribe(data => {
      let jsn = data.json();
      if(!jsn.errCode){
        console.log("data: ",data.json());
        this.station[ind].rate = i;
        this.stationCopy[ind].rate = i;
      }else {
        console.log("Token Expired");
        this.logOut();
      }
    });
  }
  editGame(i,ind){
    this.searchOpt.forEach(itm=>{
      this.gameFildVl[itm] = i[itm]
    });
    this.showGameModel = true;
    this.edContxt = {
      isEditing:true,
      itm:i,
      ind:ind
    };
  }
  addGame(){
    console.log("gameFildVl: ",this.gameFildVl);
    let isEditing = this.edContxt.isEditing;
    if(isEditing){
      this.searchOpt.forEach(itm=>{
        this.edContxt.itm[itm] = this.gameFildVl[itm];
      });
    }
    let hdr = new Headers();
    hdr.append('jwt', this.isLogin);
    this.http.post('/user/save_station',
      {station:isEditing?this.edContxt.itm:[this.gameFildVl]},
      {headers:hdr}).subscribe(data => {
      let jsn = data.json();
      if(!jsn.errCode){
        console.log("data: ",data.json());
        if(!isEditing){
          this.station.push(this.gameFildVl);
          this.stationCopy.push(this.gameFildVl);
        }else {
          this.searchOpt.forEach(itm=>{
            this.station[this.edContxt.ind][itm] = this.gameFildVl[itm]
            this.stationCopy[this.edContxt.ind][itm] = this.gameFildVl[itm]
          });
        }
      }else {
        console.log("Token Expired");
        this.logOut();
      }
      this.showGameModel = false;
      this.edContxt.isEditing = false;
    });
  }
  deleteGame(id,ind){
    console.log("id,ind: ",id,ind);

    let hdr = new Headers();
    hdr.append('jwt', this.isLogin);
    this.http.post('/user/delete_station',
      {_id:id},{headers:hdr}).subscribe(data => {
      let jsn = data.json();
      if(!jsn.errCode){
        console.log("data: ",data.json());
        this.station.splice(ind,1);
        this.stationCopy.splice(ind,1);
      }else {
        console.log("Token Expired");
        this.logOut();
      }
      this.showGameModel = false;
    });
  }

  findStation(){
    if(!this.searchField.txt){
      this.stationCopy = JSON.parse(JSON.stringify(this.station));
    }else {
      let fieldProp1 = JSON.parse(JSON.stringify(this.station));
      let tmpArr = this.searchBy==='all'?this.searchOpt:[this.searchBy];
      tmpArr.forEach(i1=>{
        let i1Fld = this.station.map(i=>''+i[i1]);
        let xxx = this.filterUtil(this.searchField.txt,i1Fld);
        xxx.forEach(itm=>{
          if(itm.string){
            fieldProp1[itm.index][i1] = itm.string
          }
        });
      });

      this.stationCopy = fieldProp1.filter(i=>{
        let isMatch = false;
        tmpArr.forEach(i1=>{
          let tst = ''+i[i1];
          if(tst && tst.includes('<b>')){
            isMatch = true;
          }
        });
        return isMatch;
      });
    }
  }

  filterUtil(pattern, arr){
    console.log("this.searchFuzzy: ",this.searchFuzzy);
    let fuzzyMatch = (pattern, str) => {
      let patternIdx = 0, result = [], len = str.length,
        totalScore = 0, currScore = 0, ch;
      for(let idx = 0; idx < len; idx++) {
        ch = str[idx];
        if(str[idx] === pattern[patternIdx]) {
          ch = '<b>' + ch + '</b>';
          patternIdx += 1;
          currScore += 1 + currScore;
        } else {
          currScore = 0;
        }
        totalScore += currScore;
        result[result.length] = ch;
      }
      if(patternIdx === pattern.length) {
        totalScore = (str === pattern) ? Infinity : totalScore;
      }
      if(patternIdx === pattern.length){
        return {
          rendered: result.join(''), score: totalScore
        }
      }
      return str;
    };
    let simpleMatch = (pattern, str)=>{
      let tmpStr = "";
      let splted = str.split(pattern);
      splted.forEach((i,ind)=>{
        if(ind===0){
          tmpStr+=(i+'<b>'+pattern)
        }else if(ind===splted.length-1){
          tmpStr+=('</b>'+i)
        }else {
          tmpStr+=('</b>'+i+'<b>'+pattern)
        }
      });
      return {
        rendered: splted.length>1?tmpStr:str, score: str.includes(pattern)?1:0
      }
    };
    if(!arr || arr.length === 0) {
      return [];
    }
    if (typeof pattern !== 'string') {
      return arr;
    }
    return arr.reduce((prev, curr, idx)=>{
      let rendered = this.searchFuzzy?fuzzyMatch(pattern, curr):simpleMatch(pattern, curr);
      if(rendered != null) {
        prev[prev.length] = {
          string: rendered.rendered, score: rendered.score, index: idx, original: curr
        };
      }
      return prev;
    }, [])
      .sort(function(a,b) {
        var compare = b.score - a.score;
        if(compare) return compare;
        return a.index - b.index;
      });
  }
  buyGame(itm){
    localStorage.setItem('itmToBuy',JSON.stringify(itm))
    this.router.navigateByUrl('cashier');
  }

  logOut(){
    localStorage.removeItem('myJWT');
    this.router.navigateByUrl('auth')
  }
}
